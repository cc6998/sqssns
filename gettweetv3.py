from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import tweepy
import logging
import json
import boto.sqs,boto.sns
from boto.sqs.message import Message
from threading import Thread
from alchemyapi import AlchemyAPI
from HTMLParser import HTMLParser
from elasticsearch import Elasticsearch
from multiprocessing import Pool
import sys

#logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
topicarn = "arn:aws:sns:us-west-2:814864085739:tweet"
es = Elasticsearch()
alchemyapi = AlchemyAPI()

sqs = boto.sqs.connect_to_region(
        "us-west-2",
        aws_access_key_id="**********your_aws_access_key_id**************",
       aws_secret_access_key="***********yout_aws_secret_access_key***********")
my_queue = sqs.get_queue('myqueue') or sqs.create_queue('myqueue')

sns = boto.sns.connect_to_region(
        "us-west-2")

class listener(StreamListener):
    #i=1
    def on_data(self, data):
        try:
            data=json.loads(data)
        
            if data['coordinates'] and data['lang']=='en':
                lon, lat = data['coordinates']['coordinates']
                cont = data['user']['description']
                id_str = data['id_str']
                m = Message()
                loc={}
                loc['latitude']=lat
                loc['longitude']=lon
                loc['content']=cont
                loc['id_str']=id_str;
                loc_json=json.dumps(loc)
                #add new tweet to sqs
                m.set_body(loc_json)
                my_queue.write(m)
            
        	#print "write to SQS:"
            #print m.get_body()

            #es.index(index='tweet',doc_type='json',body=loc_json)
        except Exception, e:
            pass

    def on_error(self, status):
        print status

def gettweet():
    while True:
        try:
            auth = tweepy.OAuthHandler('*******','*******')
            auth.set_access_token('************', '**************')

            twitterStream = Stream(auth, listener())
            #print 'after create streams'
            twitterStream.filter(track=["music","weather","company","good","and"])
        except KeyboardInterrupt:  # on Ctrl-C, break
            break
        except BaseException as e:
            print e
            pass


def senti_analyse():
        print 'worker start'
        while True:
            rs = my_queue.get_messages()
            msg_json = {}
            index = 0
            try:
            	while(len(rs)>0 and (index+1)<=len(rs)):
                	msg_str = rs[index].get_body()
                #my_queue.delete_message(rs[0])
                	encode_json = json.loads(msg_str)
                #body = msg_str.split("\"")
                #lat = body[2]
                #msg_json["lat"]=lat 
                #content = body[3]
                #msg_json['content']=content
                #id_str = body[5]
                #msg_json["id"] = id_str  
                #longi = body[7]
                #msg_json['longitude']=longi    
                	#print "encode",encode_json
                	latitude=encode_json['latitude']
                	longitude=encode_json['longitude']
                	content=encode_json['content']
                	id_str=encode_json['id_str']

               		msg_json['latitude']=latitude
                	msg_json['longitude']=longitude
                	msg_json['content']=content
                	msg_json['id_str']=id_str

                	senti = alchemyapi.sentiment("text", content)
               		#print "senti:", senti
                	if 'docSentiment' not in senti:
                            print "docSentiment not in reponse"
                            msg_json['sentiment'] = "neutral"
                	else:
                            msg_json['sentiment']=senti['docSentiment']['type']

                        data = json.dumps(msg_json)
                        #print "here"
                        es.index(index='tweet',doc_type='json',body=data)
                        index = index + 1
                #print data
                	data = data.encode("utf8")
                	sns.publish(topicarn, message=data)
                	print "data:", data
            except Exception, e:
            	pass

thread = Thread(target=gettweet)
thread.start()

threads = []
for i in range(2):
    t = Thread(target=senti_analyse)
    #t = Thread(target=gettweet)
    threads.append(t)
    t.start()


   
